export const environment = {
  production: true
};

export const settings = {
  MAIN_API: 'https://alexlaznevoy.dev-pdp.lanars.com/api/',
  SOCKET_API: 'https://alexlaznevoy.dev-pdp.lanars.com/',
};

