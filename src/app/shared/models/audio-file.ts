export class AudioFIle {
  readonly id: number;
  originalname: string;
  filename: string;
  author: string;
  imageUrl: string;
  destination: string;
  created_at: string;
  updated_at: string;
}
