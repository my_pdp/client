export class User {
  readonly id: number;
  email: string;
  password: string;
  created_at: string;
  updated_at: string;
}
