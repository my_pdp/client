import { AudioFIle } from '../models/audio-file';
import { AUDIO_STATE } from '../enums/enums';

export interface AudioData {
  audio: AudioFIle;
  filedata: object;
  state: AUDIO_STATE;
  size: number;
  progresSize: number;
}
