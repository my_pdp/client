export enum EVENT {
  CONNECT = 'connect',
  DISCONNECT = 'disconnect',
  TEST = 'test',
  GET_FILE = 'get-file',
  TRACK_STREAM = 'track-stream',
}

export enum AUDIO_STATE {
  PLAY = 'play',
  PAUSE = 'pause',
  STOP = 'stop',
  INIT = 'init',
}

