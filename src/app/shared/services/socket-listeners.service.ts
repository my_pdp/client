import { Injectable } from '@angular/core';
import { SocketService } from './socket.service';
import { EVENT } from '../enums/enums';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SocketListenersService {

  private _connected = new Subject<void>();
  private _disconnected = new Subject<void>();
  private _start = new Subject();
  private _ready = new Subject();
  private _chunk = new Subject();

  get ready() {
    return this._ready.asObservable();
  }

  get chunk() {
    return this._chunk.asObservable();
  }

  get start() {
    return this._start.asObservable();
  }

  get isConnected() {
    return this._connected.asObservable();
  }

  get isDisconnected() {
    return this._disconnected.asObservable();
  }

  constructor(
    private socketService: SocketService,
  ) { }

  /**
   * Connect
   */
  connect() {
    this.socketService.init();

    this.socketService.socketConnection.on(EVENT.CONNECT, () => {
      this._connected.next();
      this.initEventsListeners();
    });
  }

  /**
   * Disconnect
   */
  disconnect() {
    this.socketService.disconnect();
  }

  /**
   * All listeners
   */
  private initEventsListeners() {
    this.socketService.socketConnection.on('audio-end', () => {
      this._ready.next();
    });

    this.socketService.socketConnection.on('audio-start', (data) => {
      this._start.next(data);
    });

    this.socketService.socketConnection.on('audio-chunk', (data) => {
      this._chunk.next(data);
    });

    this.socketService.socketConnection.on(EVENT.DISCONNECT, () => {
      console.log('TCL: initEventsListeners -> _disconnected');
      this._disconnected.next();
    });
  }

}

