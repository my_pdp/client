import { TestBed } from '@angular/core/testing';

import { AudioService } from './audio.service';
import { HttpClientModule } from '@angular/common/http';

describe('AudioService', () => {
  let service: AudioService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule
      ]
    });
    service = TestBed.inject(AudioService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
