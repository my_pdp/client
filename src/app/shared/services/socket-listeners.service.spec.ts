import { TestBed } from '@angular/core/testing';

import { SocketListenersService } from './socket-listeners.service';

describe('SocketListenersService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SocketListenersService = TestBed.get(SocketListenersService);
    expect(service).toBeTruthy();
  });
});
