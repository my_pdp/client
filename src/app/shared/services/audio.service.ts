import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';

import { settings } from 'src/environments/environment';
import { AudioFIle } from '../models/audio-file';

@Injectable({
  providedIn: 'root'
})
export class AudioService {

  private _audioList$ = new BehaviorSubject<AudioFIle[]>(null);

  get audioList$() {
    return this._audioList$.asObservable();
  }

  constructor(
    private http: HttpClient,
  ) {
    this.loadAudio();
  }

  protected httpOptions = {
    withCredentials: false,
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  getAllAudio() {
    return this.http.get(`${settings.MAIN_API}file`, this.httpOptions);
  }

  getAudioById(id: number) {
    return this.http.get(`${settings.MAIN_API}file/${id}`, {
      ...this.httpOptions,
      responseType: 'arraybuffer'
    });
  }

  uploadFile(data: FormData) {
    return this.http.post(`${settings.MAIN_API}file`, data);
  }

  loadAudio() {
    this.getAllAudio()
      .subscribe((response: AudioFIle[]) => {
        this._audioList$.next(response);
      });
  }
}
