import { TestBed } from '@angular/core/testing';

import { AudioHelpersService } from './audio-helpers.service';

describe('AudioHelpersService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AudioHelpersService = TestBed.get(AudioHelpersService);
    expect(service).toBeTruthy();
  });
});
