import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject } from 'rxjs';

import { AudioHelpersService } from './audio-helpers.service';
import { SocketListenersService } from './socket-listeners.service';

import { AUDIO_STATE } from '../enums/enums';
import { AudioFIle } from '../models/audio-file';
import { AudioData } from '../interfaces/audio-data';

@Injectable({
  providedIn: 'root'
})
export class PlayerService {

  private _play = new BehaviorSubject<AudioFIle>(null);
  private _stop = new Subject();
  private _playState = new BehaviorSubject<AudioData>(null);
  private _progress = new BehaviorSubject<number>(null);

  get play() {
    return this._play.asObservable();
  }

  get stop() {
    return this._stop.asObservable();
  }

  get playState() {
    return this._playState.asObservable();
  }

  get progress() {
    return this._progress.asObservable();
  }

  audioContext;
  source;

  audioChunks = new Map();

  audioData: AudioData;

  constructor(
    private audioHelpersService: AudioHelpersService,
    private socketListenersService: SocketListenersService,
  ) {
    this.audioContext = this.audioHelpersService.getAudioContext();

    if (this.audioContext) {
      this.startListeners();
    }
  }

  /**
   * Start all listeners
   */
  startListeners() {
    this.socketListenersService.start
      .subscribe((data: any) => {
        if (this.audioData && this.audioData.state === AUDIO_STATE.PLAY) {
          this.stopPlaying();
        }

        this.setDefault(data);
      });

    // listen new pars
    this.socketListenersService.chunk
      .subscribe((chunk: any) => {
        this.audioContext.decodeAudioData(chunk.data, async(buffer) => {
          const source = await this.audioContext.createBufferSource();
          source.buffer = buffer;
          source.connect(this.audioContext.destination);

          this.audioChunks.set(chunk.index, source);

          if (this.audioChunks.has(1) && this.audioData.state === AUDIO_STATE.INIT) {
            this.startPlaying();
          }
        });
      });
  }

  /**
   * Play chunk
   */
  private playChunk(index = 1) {
    this.source = this.audioChunks.get(index);

    if (this.source === undefined) {
      this.stopPlaying();
      return;
    }

    this.source.start();

    this.source.onended = (ended) => {
      const length = ended.srcElement.buffer.length;

      this.audioData.progresSize += length;

      this._progress.next(this.audioHelpersService.getProgress(this.audioData.progresSize, this.audioData.size));

      index += 1;

      if (this.audioData.state === AUDIO_STATE.PLAY) {
        this.playChunk(index);
      }
    };
  }

  /**
   * Stop playing
   */
  stopPlaying() {
    this.audioData.state = AUDIO_STATE.STOP;
    this._stop.next();
    this._playState.next(this.audioData);
    this.source.stop();
  }

  /**
   * Start playing
   */
  startPlaying() {
    this.audioData.state = AUDIO_STATE.PLAY;
    this._play.next(this.audioData.audio);
    this._playState.next(this.audioData);
    this.playChunk();
  }

  /**
   * Set default
   * @param data
   */
  setDefault(data: any) {
    this.audioChunks = new Map();

    this.audioData = {
      audio: data.audio,
      filedata: data.filedata,
      state: AUDIO_STATE.INIT,
      size: data.filedata.size,
      progresSize: 0
    };

    console.log('TCL: this.audioData', this.audioData);
  }

}
