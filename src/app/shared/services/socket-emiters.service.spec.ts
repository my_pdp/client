import { TestBed } from '@angular/core/testing';

import { SocketEmitersService } from './socket-emiters.service';

describe('SocketEmitersService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SocketEmitersService = TestBed.get(SocketEmitersService);
    expect(service).toBeTruthy();
  });
});
