import { Injectable } from '@angular/core';

declare global {
  interface Window {
    AudioContext: any;
    webkitAudioContext: any;
  }
}

@Injectable({
  providedIn: 'root'
})
export class AudioHelpersService {

  constructor() { }

  /**
   * Return audio context
   */
  getAudioContext(): AudioContext {
    const AudioContext = window.AudioContext || window.webkitAudioContext;
    const audioContent = new AudioContext();
    return audioContent;
  }

  /**
   * Get progress in persent
   * @param loadedSize
   * @param size
   */
  getProgress(loadedSize, size) {
    const persent = Math.round((loadedSize / size) * 100);
    return persent;
  }

}
