import { Injectable } from '@angular/core';
import * as socketClient from 'socket.io-client';

import { settings } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SocketService {

  private _socketClient;

  get socketConnection() {
    return this._socketClient;
  }

  constructor() {
  }

  /**
   * Init socket connection
   * @param options
   */
  init() {
    this._socketClient = socketClient(`${settings.SOCKET_API}`, {
      transports: ['websocket', 'htmlfile', 'xhr-polling', 'jsonp-polling', 'polling'],
    });
  }

  connect() {
    this._socketClient.connect();
  }

  disconnect() {
    this._socketClient.disconnect();
  }

}
