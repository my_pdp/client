import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { settings } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  protected httpOptions = {
    withCredentials: false,
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  constructor(
    private http: HttpClient
  ) { }

  getUser() {
    return this.http.get(`${settings.MAIN_API}user`, this.httpOptions);
  }

}
