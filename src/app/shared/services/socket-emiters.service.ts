import { Injectable } from '@angular/core';
import { EVENT } from '../enums/enums';
import { SocketService } from './socket.service';

@Injectable({
  providedIn: 'root'
})
export class SocketEmitersService {

  constructor(
    private socketService: SocketService,
  ) { }

  /**
   * Emit file
   */
  getAudioFile(audio) {
    this.socketService.socketConnection.emit(EVENT.GET_FILE, audio);
  }

}
