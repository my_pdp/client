import { TestBed } from '@angular/core/testing';

import { PromptUpdateService } from './prompt-update.service';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from 'src/environments/environment';

describe('PromptUpdateService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    ],
  }));

  it('should be created', () => {
    const service: PromptUpdateService = TestBed.get(PromptUpdateService);
    expect(service).toBeTruthy();
  });
});
