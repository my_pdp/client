import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { of, throwError } from 'rxjs';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

import { LoginComponent } from './login.component';
import { LoginService } from '../shared/services/login.service';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let loginService: LoginService;
  const formGroup: FormGroup = new FormGroup({});

  const MockRouter = {
    navigate: jasmine.createSpy('navigate'),
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
      ],
      declarations: [
        LoginComponent
      ],
      providers: [
        LoginService,
        {
          provide: Router,
          useValue: MockRouter,
        },
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    loginService = TestBed.inject(LoginService);

    spyOn(localStorage, 'setItem');

    spyOn(component, 'handleRequestErrors');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // login
  describe('login()', () => {

    it('should redirect to user page', () => {
      spyOn(loginService, 'login').and.returnValue(of('token'));

      component.login(formGroup);
      fixture.detectChanges();

      expect(MockRouter.navigate).toHaveBeenCalledWith(['user']);
    });

    it('should set token to local storage', () => {
      spyOn(loginService, 'login').and.returnValue(of('token'));

      component.login(formGroup);
      fixture.detectChanges();

      expect(localStorage.setItem).toHaveBeenCalledWith('access_token', 'token');
    });

    it('should catch error if subscrive throw error', () => {
      spyOn(loginService, 'login').and.returnValue(throwError(''));

      component.onSubmit();

      expect(component.handleRequestErrors).toHaveBeenCalled();
    });
  });
});
