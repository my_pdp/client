import { Component, OnInit, OnDestroy, ChangeDetectionStrategy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil, finalize } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';

import { LoginService } from '../shared/services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoginComponent implements OnInit, OnDestroy {

  private destroy$ = new Subject<void>();
  loginForm: FormGroup;

  get email () {
    return this.loginForm.get('email');
  }

  get password () {
    return this.loginForm.get('password');
  }

  constructor(
    private router: Router,
    private loginService: LoginService
  ) { }

  ngOnInit() {
    this.initForm();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  /**
   * Init login form group
   */
  initForm() {
    this.loginForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required, Validators.minLength(6)])
    });
  }

  onSubmit() {
    this.login(this.loginForm.value);
  }

  /**
   * Login user
   * @param data
   */
  login(data: FormGroup) {
    this.loginForm.disable();

    this.loginService.login(data)
      .pipe(
        finalize(() => {
          this.loginForm.enable();
        }),
        takeUntil(this.destroy$),
      )
      .subscribe((access_token: string) => {
        localStorage.setItem('access_token', access_token);
        this.router.navigate(['user']);
      }, (error) => {
        this.handleRequestErrors(error);
      });
  }

  /**
   * Catch http errors
   * @param error
   */
  handleRequestErrors(error: HttpErrorResponse) {
    console.error(error);
  }

}
