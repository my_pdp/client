import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';

import { AuthenticationRoutingModule } from './authentication-routing.module';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AuthenticationHostComponent } from './authentication-host/authentication-host.component';


@NgModule({
  declarations: [
    LoginComponent,
    RegisterComponent,
    AuthenticationHostComponent
  ],
  imports: [
    AuthenticationRoutingModule,
    SharedModule
  ]
})
export class AuthenticationModule { }
