import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { of, throwError } from 'rxjs';

import { RegisterComponent } from './register.component';
import { RegisterService } from '../shared/services/register.service';

describe('RegisterComponent', () => {
  let component: RegisterComponent;
  let fixture: ComponentFixture<RegisterComponent>;
  let registerService: RegisterService;

  const formGroup: FormGroup = new FormGroup({});

  const MockRouter = {
    navigate: jasmine.createSpy('navigate'),
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
      ],
      declarations: [
        RegisterComponent
      ],
      providers: [
        RegisterService,
        {
          provide: Router,
          useValue: MockRouter,
        },
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    registerService = TestBed.inject(RegisterService);

    spyOn(localStorage, 'setItem');

    spyOn(component, 'handleRequestErrors');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // login
  describe('register()', () => {

    it('should redirect to user page', () => {
      spyOn(registerService, 'register').and.returnValue(of('token'));

      component.register(formGroup);
      fixture.detectChanges();

      expect(MockRouter.navigate).toHaveBeenCalledWith(['user']);
    });

    it('should set token to local storage', () => {
      spyOn(registerService, 'register').and.returnValue(of('token'));

      component.register(formGroup);
      fixture.detectChanges();

      expect(localStorage.setItem).toHaveBeenCalledWith('access_token', 'token');
    });

    it('should catch error if subscrive throw error', () => {
      spyOn(registerService, 'register').and.returnValue(throwError(''));

      component.onSubmit();

      expect(component.handleRequestErrors).toHaveBeenCalled();
    });
  });
});
