import { Component, OnInit, OnDestroy, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { takeUntil, finalize } from 'rxjs/operators';

import { RegisterService } from '../shared/services/register.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RegisterComponent implements OnInit, OnDestroy {

  private destroy$ = new Subject<void>();
  registerForm: FormGroup;

  constructor(
    private router: Router,
    private registerService: RegisterService
  ) { }

  get email () {
    return this.registerForm.get('email');
  }

  get password () {
    return this.registerForm.get('password');
  }

  ngOnInit() {
    this.initForm();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  /**
   * Init register form group
   */
  initForm() {
    this.registerForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required, Validators.minLength(6)])
    });
  }

  onSubmit() {
    this.register(this.registerForm.value);
  }

  /**
   * Send reqiest to register
   * @param data
   */
  register(data: FormGroup) {
    this.registerForm.disable();

    this.registerService.register(data)
      .pipe(
        finalize(() => {
          this.registerForm.enable();
        }),
        takeUntil(this.destroy$),
      )
      .subscribe((access_token: string) => {
        localStorage.setItem('access_token', access_token);
        this.router.navigate(['user']);
      }, (error) => {
        this.handleRequestErrors(error);
      });
  }

  /**
   * Catch http errors
   * @param error
   */
  handleRequestErrors(error: HttpErrorResponse) {
    console.error(error);
  }
}
