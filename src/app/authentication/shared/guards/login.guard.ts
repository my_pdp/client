import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanActivate {
  constructor (
    private route: Router,
    private jwtHelperService: JwtHelperService,
  ) {}

  canActivate() {
    const accessToken = this.jwtHelperService.tokenGetter();

    if (accessToken) {
      this.route.navigate(['user']);
      return false;
    }

    return true;
  }
}
