import { TestBed, inject } from '@angular/core/testing';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';

import { LoginGuard } from './login.guard';
import { RouterTestingModule } from '@angular/router/testing';

describe('LoginGuard', () => {
  const MockJwtHelperService = {
    tokenGetter: jasmine.createSpy('tokenGetter'),
  };

  const MockRouter = {
    navigate: jasmine.createSpy('navigate'),
  };

  let loginGuard: LoginGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
      ],
      providers: [
        LoginGuard,
        {
          provide: Router,
          useValue: MockRouter,
        },
        {
          provide: JwtHelperService,
          useValue: MockJwtHelperService,
        }
      ]
    });

    loginGuard = TestBed.inject(LoginGuard);
  });

  it('should be created', inject([LoginGuard], (guard: LoginGuard) => {
    expect(guard).toBeTruthy();
  }));

  // canActivate
  describe('canActivate()', () => {

    it('should return true if token is empy', () => {
      MockJwtHelperService.tokenGetter.and.returnValue('');

      expect(loginGuard.canActivate()).toBe(true);
    });

    it('should return false if token is not empy', () => {
      MockJwtHelperService.tokenGetter.and.returnValue('token');

      expect(loginGuard.canActivate()).toBe(false);
      expect(MockRouter.navigate).toHaveBeenCalledWith(['user']);
    });
  });
});
