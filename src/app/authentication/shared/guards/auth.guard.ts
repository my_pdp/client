import { Injectable } from '@angular/core';
import { CanActivateChild } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';

import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivateChild {
  constructor (
    private jwtHelperService: JwtHelperService,
    private authService: AuthService,
  ) {}

  canActivateChild() {
    const accessToken = this.jwtHelperService.tokenGetter();

    if (!accessToken) {
      this.logout();
      return false;
    }

    if (this.jwtHelperService.isTokenExpired()) {
      this.logout();
      return false;
    }

    return true;
  }

  logout() {
    this.authService.exit();
  }

}
