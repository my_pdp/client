import { TestBed, inject } from '@angular/core/testing';

import { AuthGuard } from './auth.guard';
import { AuthService } from '../services/auth.service';
import { JwtHelperService } from '@auth0/angular-jwt';

describe('AuthGuard', () => {
  const MockJwtHelperService = {
    isTokenExpired: jasmine.createSpy('isTokenExpired'),
    tokenGetter: jasmine.createSpy('tokenGetter'),
  };

  const MockAuthService = {
    exit: jasmine.createSpy('exit'),
  };

  let authGuard: AuthGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AuthGuard,
        {
          provide: AuthService,
          useValue: MockAuthService,
        },
        {
          provide: JwtHelperService,
          useValue: MockJwtHelperService,
        },
      ]
    });

    authGuard = TestBed.inject(AuthGuard);
    spyOn(authGuard, 'logout');
  });

  it('should be created', inject([AuthGuard], (guard: AuthGuard) => {
    expect(guard).toBeTruthy();
  }));

  // canActivateChild
  describe('canActivateChild()', () => {
    it('should return false if token is empty', inject([AuthGuard], (guard: AuthGuard) => {
      MockJwtHelperService.tokenGetter.and.returnValue('');

      expect(guard.canActivateChild()).toBe(false);
      expect(authGuard.logout).toHaveBeenCalled();
    }));

    it('should return false if token is expired', inject([AuthGuard], (guard: AuthGuard) => {
      MockJwtHelperService.tokenGetter.and.returnValue('token');
      MockJwtHelperService.isTokenExpired.and.returnValue(true);

      expect(guard.canActivateChild()).toBe(false);
      expect(authGuard.logout).toHaveBeenCalled();
    }));

    it('should return true if token is valid', inject([AuthGuard], (guard: AuthGuard) => {
      MockJwtHelperService.tokenGetter.and.returnValue('token');
      MockJwtHelperService.isTokenExpired.and.returnValue(false);

      expect(guard.canActivateChild()).toBe(true);
    }));
  });

});
