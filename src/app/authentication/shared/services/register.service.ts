import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { settings } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  protected httpOptions = {
    withCredentials: false,
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  constructor(
    private http: HttpClient
  ) { }

  register(data) {
    return this.http.post(`${settings.MAIN_API}register`, data, this.httpOptions)
      .pipe(
        map((result: any) => {
          return result.access_token;
        }),
      );
  }
}
