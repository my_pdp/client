import { TestBed } from '@angular/core/testing';

import { AuthService } from './auth.service';
import { Router } from '@angular/router';

describe('AuthService', () => {
  const MockRouter = {
    navigate: jasmine.createSpy('navigate'),
  };
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      {
        provide: Router,
        useValue: MockRouter,
      },
    ]
  }));

  it('should be created', () => {
    const service: AuthService = TestBed.get(AuthService);
    expect(service).toBeTruthy();
  });
});
