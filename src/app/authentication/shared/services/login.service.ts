import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { settings } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  protected httpOptions = {
    withCredentials: false,
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  constructor(
    private http: HttpClient
  ) { }

  login(data) {
    return this.http.post(`${settings.MAIN_API}login`, data, this.httpOptions)
      .pipe(
        map((result: any) => {
          return result.access_token;
        }),
      );
  }
}
