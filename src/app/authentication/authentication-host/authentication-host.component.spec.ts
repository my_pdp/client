import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthenticationHostComponent } from './authentication-host.component';

describe('AuthenticationHostComponent', () => {
  let component: AuthenticationHostComponent;
  let fixture: ComponentFixture<AuthenticationHostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthenticationHostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthenticationHostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
