import { Component } from '@angular/core';
import { PromptUpdateService } from './shared/services/prompt-update.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  constructor(
    // service for PWA auto update
    public promptUpdateService: PromptUpdateService,
  ) {
  }
}
