import { Component, OnInit } from '@angular/core';
import { SocketListenersService } from 'src/app/shared/services/socket-listeners.service';
import { RouterOutlet } from '@angular/router';
import { slideInAnimation } from 'src/app/shared/animation/animation';

@Component({
  selector: 'app-user-host',
  templateUrl: './user-host.component.html',
  styleUrls: ['./user-host.component.scss'],
  animations: [
    slideInAnimation
  ]
})
export class UserHostComponent implements OnInit {

  constructor(
    private socketListenersService: SocketListenersService,
  ) { }

  ngOnInit() {
    this.socketListenersService.connect();
  }

  prepareRoute(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
  }

}
