import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { of } from 'rxjs';

import { AddFileComponent } from './add-file.component';
import { AudioService } from 'src/app/shared/services/audio.service';

describe('AddFileComponent', () => {
  let component: AddFileComponent;
  let fixture: ComponentFixture<AddFileComponent>;
  let audioService: AudioService;
  const formData: FormData = new FormData();

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
      ],
      declarations: [
        AddFileComponent
      ],
      providers: [
        AudioService,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddFileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    audioService = TestBed.inject(AudioService);

    spyOn(component, 'clearFormData');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // uploadFile
  describe('uploadFile()', () => {

    it('should clear form after file was loaded', () => {
      spyOn(audioService, 'uploadFile').and.returnValue(of(true));

      component.uploadFile(formData);

      fixture.detectChanges();

      expect(component.clearFormData).toHaveBeenCalled();
    });
  });
});
