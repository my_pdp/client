import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { AudioService } from 'src/app/shared/services/audio.service';

@Component({
  selector: 'app-add-file',
  templateUrl: './add-file.component.html',
  styleUrls: ['./add-file.component.scss'],
})
export class AddFileComponent implements OnInit, OnDestroy {

  private disable$ = new Subject<void>();

  @ViewChild('fileInput')
  fileInput: ElementRef;

  fileFormGroup: FormGroup;

  get file() {
    return this.fileFormGroup.get('file');
  }

  constructor(
    private audioService: AudioService,
  ) { }

  ngOnInit() {
    this.formInit();
  }

  ngOnDestroy() {
    this.disable$.next();
    this.disable$.complete();
  }

  formInit() {
    this.fileFormGroup = new FormGroup({
      file: new FormControl(null, [Validators.required])
    });
  }

  /**
   * Prepare file
   */
  submit() {
    const formData = new FormData();

    formData.append('file', this.file.value);

    this.uploadFile(formData);
  }

  /**
   * When file was selected
   * @param event
   */
  onFileSelect(event) {
    const reader = new FileReader();

    if (event.target.files && event.target.files.length) {
      const fileData = <File>event.target.files[0];
      reader.readAsDataURL(fileData);

      reader.onload = () => {
        this.fileFormGroup.patchValue({
          file: fileData
        });
      };
    }
  }

  /**
   * Upload file
   * @param data
   */
  uploadFile(data: FormData) {
    this.audioService.uploadFile(data)
      .pipe(
        takeUntil(this.disable$),
      )
      .subscribe((response) => {
        this.clearFormData();
      }), (error) => {
        console.error(error);
      };
  }

  /**
   * Clear form with file
   */
  clearFormData() {
    this.fileFormGroup.reset();
    this.fileInput.nativeElement.value = null;
  }
}
