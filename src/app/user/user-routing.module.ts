import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserHostComponent } from './user-host/user-host.component';
import { AuthGuard } from '../authentication/shared/guards/auth.guard';
import { AudioListComponent } from './audio-list/audio-list.component';
import { AddFileComponent } from './add-file/add-file.component';
import { AudioItemComponent } from './audio-item/audio-item.component';


const routes: Routes = [
  {
    path: '',
    component: UserHostComponent,
    canActivateChild: [AuthGuard],
    children: [
      {
        path: '',
        redirectTo: 'audio-list',
        pathMatch: 'full',
      },
      {
        path: 'audio-list',
        component: AudioListComponent,
        data: {
          animation: 'AudioList'
        }
      },
      {
        path: 'audio-player',
        component: AudioItemComponent,
        data: {
          animation: 'AudioPlayer'
        }
      },
      {
        path: 'add-file',
        component: AddFileComponent,
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
