import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { of } from 'rxjs';

import { AudioMiniPlayerComponent } from './audio-mini-player.component';
import { AudioData } from 'src/app/shared/interfaces/audio-data';
import { AudioFIle } from 'src/app/shared/models/audio-file';
import { AUDIO_STATE } from 'src/app/shared/enums/enums';
import { PlayerService } from 'src/app/shared/services/player.service';

describe('AudioMiniPlayerComponent', () => {
  let component: AudioMiniPlayerComponent;
  let fixture: ComponentFixture<AudioMiniPlayerComponent>;
  let playerService: PlayerService;

  const MockRouter = {
    navigate: jasmine.createSpy('navigate'),
  };

  const audioData: AudioData = {
    audio: new AudioFIle,
    filedata: {},
    state: AUDIO_STATE.INIT,
    size: 1,
    progresSize: 1,
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AudioMiniPlayerComponent
      ],
      providers: [
        PlayerService, // TODO - если есть TestBed.inject(PlayerService), надо объявлять здесь?
        {
          provide: Router,
          useValue: MockRouter,
        },
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AudioMiniPlayerComponent);
    component = fixture.componentInstance;
    component.audioData = audioData;
    fixture.detectChanges();

    playerService = TestBed.inject(PlayerService);

    spyOn(component, 'play');
    spyOn(component, 'pause');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // listenPlayEvent
  describe('listenPlayEvent', () => {

    it('should set audio data in property', () => {
      const response: AudioData = audioData;

      spyOnProperty(playerService, 'playState').and.returnValue(of(response));

      component.listenPlayEvent();

      expect(component.audioData).toEqual(response);
    });
  });

  // changePlayState
  describe('changePlayState()', () => {

    it('should call pause() if audio is playing', () => {
      spyOnProperty(component, 'isPlaying').and.returnValue(true);

      component.changePlayState();

      expect(component.pause).toHaveBeenCalled();
    });

    it('should call play() if audio is not playing', () => {
      spyOnProperty(component, 'isPlaying').and.returnValue(false);

      component.changePlayState();

      expect(component.play).toHaveBeenCalled();
    });
  });

  // onSwipe
  describe('onSwipe()', () => {
    it('should redirect on user player screen', () => {
      component.onSwipe();

      expect(MockRouter.navigate).toHaveBeenCalledWith(['user', 'audio-player']);
    });
  });

});
