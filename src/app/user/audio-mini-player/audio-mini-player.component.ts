import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, Output, EventEmitter } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil, filter } from 'rxjs/operators';
import { Router } from '@angular/router';

import { PlayerService } from 'src/app/shared/services/player.service';
import { AudioFIle } from 'src/app/shared/models/audio-file';
import { AudioData } from 'src/app/shared/interfaces/audio-data';
import { AUDIO_STATE } from 'src/app/shared/enums/enums';

@Component({
  selector: 'app-audio-mini-player',
  templateUrl: './audio-mini-player.component.html',
  styleUrls: ['./audio-mini-player.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AudioMiniPlayerComponent implements OnInit {

  audioData: AudioData;

  private destroy$ = new Subject<void>();

  get audio(): AudioFIle {
    return this.audioData ? this.audioData.audio : null;
  }

  get isPlaying() {
    return this.audioData ? this.audioData.state === AUDIO_STATE.PLAY : false;
  }

  @Output()
  toPlay = new EventEmitter<AudioFIle>();

  @Output()
  toPause = new EventEmitter<AudioFIle>();

  constructor(
    private playerService: PlayerService,
    private router: Router,
    private changeDetectorRef: ChangeDetectorRef,
  ) { }

  ngOnInit(): void {
    this.listenPlayEvent();
  }

  /**
   * Listen when play event was emited
   */
  listenPlayEvent() {
    this.playerService.playState
      .pipe(
        filter((audioData: AudioData) => {
          return !!audioData;
        }),
        takeUntil(this.destroy$)
      )
      .subscribe((audioData: AudioData) => {
        this.audioData = audioData;
        this.changeDetectorRef.markForCheck();
      });
  }


  /**
   * Change play state
   */
  changePlayState() {
    if (this.isPlaying) {
      this.pause();
    } else {
      this.play();
    }
  }

  /**
   * Set play state
   */
  play() {
    this.toPlay.emit(this.audio);
  }

  /**
   * Set pause state
   */
  pause() {
    this.toPause.emit(this.audio);
  }

  onSwipe() {
    this.router.navigate(['user', 'audio-player']);
  }

}
