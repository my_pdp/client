import { NgModule } from '@angular/core';
import { HammerGestureConfig, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import * as Hammer from 'hammerjs';

import { UserRoutingModule } from './user-routing.module';
import { SharedModule } from '../shared/shared.module';

import { UserHostComponent } from './user-host/user-host.component';
import { AudioListComponent } from './audio-list/audio-list.component';
import { AudioListItemComponent } from './audio-list/audio-list-item/audio-list-item.component';
import { AudioListHeaderComponent } from './audio-list-header/audio-list-header.component';
import { AddFileComponent } from './add-file/add-file.component';
import { AudioMiniPlayerComponent } from './audio-mini-player/audio-mini-player.component';
import { AudioItemComponent } from './audio-item/audio-item.component';
import { AudioRecordComponent } from './audio-item/audio-record/audio-record.component';
import { TimeLineComponent } from './audio-item/time-line/time-line.component';

export class MyHammerConfig extends HammerGestureConfig {
  overrides = <any> {
    pan: { direction: Hammer.DIRECTION_ALL },
    swipe: { direction: Hammer.DIRECTION_VERTICAL },
  }
};

@NgModule({
  declarations: [
    UserHostComponent,
    AudioListComponent,
    AudioListItemComponent,
    AudioListHeaderComponent,
    AddFileComponent,
    AudioMiniPlayerComponent,
    AudioItemComponent,
    AudioRecordComponent,
    TimeLineComponent
  ],
  imports: [
    SharedModule,
    UserRoutingModule,
  ],
  providers: [
    {
      provide: HAMMER_GESTURE_CONFIG,
      useClass: MyHammerConfig,
    },
  ],
})
export class UserModule { }
