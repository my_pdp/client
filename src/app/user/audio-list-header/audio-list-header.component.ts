import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/authentication/shared/services/auth.service';

@Component({
  selector: 'app-audio-list-header',
  templateUrl: './audio-list-header.component.html',
  styleUrls: ['./audio-list-header.component.scss']
})
export class AudioListHeaderComponent implements OnInit {

  constructor(
    private authService: AuthService,
  ) { }

  ngOnInit(): void {
  }

  exit() {
    this.authService.exit();
  }

}
