import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AudioListHeaderComponent } from './audio-list-header.component';
import { AuthService } from 'src/app/authentication/shared/services/auth.service';

describe('AudioListHeaderComponent', () => {
  let component: AudioListHeaderComponent;
  let fixture: ComponentFixture<AudioListHeaderComponent>;

  const MockAuthService = {
    exit: jasmine.createSpy('exit'),
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AudioListHeaderComponent
      ],
      providers: [
        {
          provide: AuthService,
          useValue: MockAuthService,
        },
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AudioListHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // exit
  describe('exit', () => {
    it('should call exit method from authService', () => {
      component.exit();

      expect(MockAuthService.exit).toHaveBeenCalled();
    });
  })
});
