import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';

@Component({
  selector: 'app-audio-record',
  templateUrl: './audio-record.component.html',
  styleUrls: ['./audio-record.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AudioRecordComponent implements OnInit {

  @Input()
  image: string;

  rounds = [];
  roundsCount: number = 10;
  start = 165;
  step = 9;

  get imageUrl () {
    return this.image ? this.image : '';
  }

  constructor() { }

  ngOnInit(): void {
    this.generateRounds();
  }

  generateRounds() {
    this.rounds = new Array(10).fill(0).map((item, index) => {
      return this.start + this.step * index;
    });
  }

}
