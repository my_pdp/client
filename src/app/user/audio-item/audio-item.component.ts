import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Subject } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';

import { AudioData } from 'src/app/shared/interfaces/audio-data';
import { PlayerService } from 'src/app/shared/services/player.service';
import { AUDIO_STATE } from 'src/app/shared/enums/enums';
import { AudioService } from 'src/app/shared/services/audio.service';
import { AudioFIle } from 'src/app/shared/models/audio-file';

@Component({
  selector: 'app-audio-item',
  templateUrl: './audio-item.component.html',
  styleUrls: ['./audio-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AudioItemComponent implements OnInit {

  audioList: AudioFIle[];
  audioData: AudioData;

  get imageUrl() {
    return this.audioData ? this.audioData.audio.imageUrl : '';
  }

  get title() {
    return this.audioData ? this.audioData.audio.originalname : '';
  }

  get author() {
    return this.audioData ? this.audioData.audio.author : '';
  }

  get isPlaying() {
    return this.audioData.state === AUDIO_STATE.PLAY;
  }


  private destroy$ = new Subject<void>();

  constructor(
    private audioService: AudioService,
    private playerService: PlayerService,
    private changeDetectorRef: ChangeDetectorRef,
  ) { }

  ngOnInit(): void {
    this.getAudioList();
    this.listenPlayEvent();
  }

  /**
   * Get all audio
   */
  getAudioList() {
    this.audioService.audioList$
      .pipe(
        filter((audioList: AudioFIle[]) => {
          return !!audioList;
        }),
        takeUntil(this.destroy$),
      )
      .subscribe((audioList: AudioFIle[]) => {
        this.audioList = audioList;

        const data = {
          audio: this.audioList[0],
          filedata: {},
          state: AUDIO_STATE.INIT,
          size: 0,
          progresSize: 0
        };

        if (!this.audioData) {
          this.audioData = data;
        }

        this.changeDetectorRef.markForCheck();
      });
  }

  /**
   * Listen when play event was emited
   */
  listenPlayEvent() {
    this.playerService.playState
      .pipe(
        filter((audioData: AudioData) => {
          return !!audioData;
        }),
        takeUntil(this.destroy$)
      )
      .subscribe((audioData: AudioData) => {
        this.audioData = audioData;

        this.changeDetectorRef.markForCheck();
      });
  }

  /**
   * Change play state
   */
  changePlayState() {
    if (this.isPlaying) {
      this.pause();
    } else {
      this.play();
    }
  }

  /**
   * Set play state
   */
  play() {
  }

  /**
   * Set pause state
   */
  pause() {
  }


}
