import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { PlayerService } from 'src/app/shared/services/player.service';
import { Subject } from 'rxjs';
import { takeUntil, filter } from 'rxjs/operators';

@Component({
  selector: 'app-time-line',
  templateUrl: './time-line.component.html',
  styleUrls: ['./time-line.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TimeLineComponent implements OnInit {

  timeLine = [];
  timeLineHeights = [
    10, 18, 29, 15, 10,
    26, 16, 10, 16, 24,
    37, 24, 15, 10, 24,
    33, 18, 13, 24, 15,
    10, 29, 12, 24, 10,
    24, 13, 6
  ];

  private destroy$ = new Subject<void>();

  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    private playerService: PlayerService,
  ) { }

  ngOnInit(): void {
    this.generateTimeLine();
    this.listenProgress();
  }

  /**
   * Generate time line
   */
  generateTimeLine() {
    this.timeLine = this.timeLineHeights.map((item) => {
      return {
        height: item,
        active: false,
      };
    });
  }

  /**
   * Set time line percent
   * @param persent
   */
  setPersent(persent: number) {
    const persentToCount = Math.round((persent / 100) * this.timeLineHeights.length);

    for (let i = 0; i <= persentToCount; i++) {
      this.timeLine[i].active = true;
    }

    this.changeDetectorRef.markForCheck();
  }

  /**
   * Listen when progress was changed
   */
  listenProgress() {
    this.playerService.progress
      .pipe(
        filter((persent: number) => {
          return !!persent;
        }),
        takeUntil(this.destroy$),
      )
      .subscribe((persent: number) => {
        this.setPersent(persent);

        this.changeDetectorRef.markForCheck();
      });
  }

}
