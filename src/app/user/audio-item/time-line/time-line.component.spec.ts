import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimeLineComponent } from './time-line.component';
import { PlayerService } from 'src/app/shared/services/player.service';
import { of } from 'rxjs';

describe('TimeLineComponent', () => {
  let component: TimeLineComponent;
  let fixture: ComponentFixture<TimeLineComponent>;
  let playerService: PlayerService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        TimeLineComponent
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimeLineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    playerService = TestBed.inject(PlayerService);

    spyOn(component, 'setPersent');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // listenProgress
  describe('listenProgress()', () => {

    it('should call setPersent method', () => {
      const response = 1;

      spyOnProperty(playerService, 'progress').and.returnValue(of(response));

      component.listenProgress();

      expect(component.setPersent).toHaveBeenCalledWith(response);
    });
  });
});
