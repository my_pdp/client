import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpClientModule } from '@angular/common/http';

import { AudioItemComponent } from './audio-item.component';
import { AudioService } from 'src/app/shared/services/audio.service';
import { PlayerService } from 'src/app/shared/services/player.service';
import { AudioFIle } from 'src/app/shared/models/audio-file';
import { AudioData } from 'src/app/shared/interfaces/audio-data';
import { AUDIO_STATE } from 'src/app/shared/enums/enums';

describe('AudioItemComponent', () => {
  let component: AudioItemComponent;
  let fixture: ComponentFixture<AudioItemComponent>;
  let audioService: AudioService;
  let playerService: PlayerService;

  const audioData: AudioData = {
    audio: new AudioFIle,
    filedata: {},
    state: AUDIO_STATE.INIT,
    size: 1,
    progresSize: 1,
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
      ],
      declarations: [
        AudioItemComponent
      ],
      providers: [
        AudioService,
        PlayerService,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AudioItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    audioService = TestBed.inject(AudioService);
    playerService = TestBed.inject(PlayerService);

    spyOn(component, 'play');
    spyOn(component, 'pause');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // getAudioList
  describe('getAudioList()', () => {
    it('should set audio files in property', () => {
      const response: AudioFIle[] = [];
      spyOnProperty(audioService, 'audioList$').and.returnValue(of(response));

      component.getAudioList();

      expect(component.audioList).toEqual(response);
    });
  });

  // listenPlayEvent
  describe('listenPlayEvent()', () => {
    it('should set audio data in property', () => {
      const response: AudioData = audioData;

      spyOnProperty(playerService, 'playState').and.returnValue(of(response));

      component.listenPlayEvent();

      expect(component.audioData).toEqual(response);
    });
  });

  // changePlayState
  describe('changePlayState()', () => {

    it('should call pause() if audio is playing', () => {
      spyOnProperty(component, 'isPlaying').and.returnValue(true);

      component.changePlayState();

      expect(component.pause).toHaveBeenCalled();
    });

    it('should call play() if audio is not playing', () => {
      spyOnProperty(component, 'isPlaying').and.returnValue(false);

      component.changePlayState();

      expect(component.play).toHaveBeenCalled();
    });
  });
});
