import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil, filter } from 'rxjs/operators';
import { trigger, transition, style, animate } from '@angular/animations';

import { AudioFIle } from 'src/app/shared/models/audio-file';
import { AudioService } from 'src/app/shared/services/audio.service';
import { SocketEmitersService } from 'src/app/shared/services/socket-emiters.service';
import { PlayerService } from 'src/app/shared/services/player.service';
import { AudioData } from 'src/app/shared/interfaces/audio-data';
import { AUDIO_STATE } from 'src/app/shared/enums/enums';

@Component({
  selector: 'app-audio-list',
  templateUrl: './audio-list.component.html',
  styleUrls: ['./audio-list.component.scss'],
  animations: [
    trigger(
      'enterAnimation', [
        transition(':enter', [
          style({ transform: 'translateY(100%)', opacity: 0 }),
          animate('200ms', style({ transform: 'translateY(0)', opacity: 1 }))
        ]),
        transition(':leave', [
          style({ transform: 'translateY(0)', opacity: 1 }),
          animate('200ms', style({ transform: 'translateY(100%)', opacity: 0 }))
        ])
      ]
    )
  ],
})
export class AudioListComponent implements OnInit {

  audioList: AudioFIle[];
  audioData: AudioData;
  playingAudioId$ = new Subject<number | null>();

  private destroy$ = new Subject<void>();

  get isMiniPlayer() {
    return this.audioData ? this.audioData.state !== AUDIO_STATE.INIT : false;
  }

  constructor(
    private audioService: AudioService,
    private socketEmitersService: SocketEmitersService,
    private playerService: PlayerService,
  ) { }

  ngOnInit() {
    this.getAudioList();

    this.listenAudioData();
  }

  /**
   * Get all audio
   */
  getAudioList() {
    this.audioService.audioList$
      .pipe(
        filter((audioList: AudioFIle[]) => {
          return !!audioList;
        }),
        takeUntil(this.destroy$),
      )
      .subscribe((audioList: AudioFIle[]) => {
        this.audioList = audioList;
      });
  }

  /**
   * Play audio
   */
  playAudio(audio: AudioFIle) {
    this.socketEmitersService.getAudioFile(audio);
  }

  /**
   * Set on pause
   */
  setAudioOnPause(audio: AudioFIle) {
    this.playerService.stopPlaying();
  }

  /**
   * Listen audio data update
   */
  listenAudioData() {
    this.playerService.play
      .pipe(
        filter((audio: AudioFIle) => {
          return !!audio;
        }),
        takeUntil(this.destroy$)
      )
      .subscribe((audio: AudioFIle) => {
        this.playingAudioId$.next(audio.id);
      });

    // Emit when audio was stopped
    this.playerService.stop
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe(() => {
        this.playingAudioId$.next(null);
      });

    // listen
    this.playerService.playState
      .pipe(
        filter((audioData: AudioData) => {
          return !!audioData;
        }),
        takeUntil(this.destroy$)
      )
      .subscribe((audioData: AudioData) => {
        this.audioData = audioData;
      });
  }

}
