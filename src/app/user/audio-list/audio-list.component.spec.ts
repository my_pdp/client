import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { of } from 'rxjs';

import { AudioListComponent } from './audio-list.component';
import { AudioService } from 'src/app/shared/services/audio.service';
import { PlayerService } from 'src/app/shared/services/player.service';
import { SocketEmitersService } from 'src/app/shared/services/socket-emiters.service';
import { AudioFIle } from 'src/app/shared/models/audio-file';
import { AudioData } from 'src/app/shared/interfaces/audio-data';
import { AUDIO_STATE } from 'src/app/shared/enums/enums';

describe('AudioListComponent', () => {
  let component: AudioListComponent;
  let fixture: ComponentFixture<AudioListComponent>;
  let audioService: AudioService;
  let playerService: PlayerService;

  const audioData: AudioData = {
    audio: new AudioFIle,
    filedata: {},
    state: AUDIO_STATE.INIT,
    size: 1,
    progresSize: 1,
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
      ],
      declarations: [
        AudioListComponent
      ],
      providers: [
        AudioService,
        PlayerService,
        SocketEmitersService,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AudioListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    audioService = TestBed.inject(AudioService);
    playerService = TestBed.inject(PlayerService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // getAudioList
  describe('getAudioList', () => {

    it('should set list of audio in property', () => {
      const response: AudioFIle[] = [];

      spyOnProperty(audioService, 'audioList$').and.returnValue(of(response));

      component.getAudioList();
      fixture.detectChanges();

      expect(component.audioList).toEqual(response);
    });

    it('should not set list of audio in property', () => {
      const response: AudioFIle[] = null;

      spyOnProperty(audioService, 'audioList$').and.returnValue(of(response));

      component.getAudioList();

      fixture.detectChanges();

      expect(component.audioList).toBeUndefined();
    });
  });

  // listenAudioData
  describe('listenAudioData()', () => {

    it('should successfully complite play subscribe', () => {
      const response: AudioFIle = new AudioFIle();

      spyOnProperty(playerService, 'play').and.returnValue(of(response));

      component.listenAudioData();
      fixture.detectChanges();

      // expect. TODO
    });

    it('should successfully complite stop subscribe', () => {
      const response: AudioFIle = new AudioFIle();

      spyOnProperty(playerService, 'stop').and.returnValue(of(response));

      component.listenAudioData();
      fixture.detectChanges();

      // expect. TODO
    });

    it('should set the audio data', () => {
      const response: AudioData = audioData;

      spyOnProperty(playerService, 'playState').and.returnValue(of(response));

      component.listenAudioData();
      fixture.detectChanges();

      expect(component.audioData).toEqual(response);
    });
  });
});
