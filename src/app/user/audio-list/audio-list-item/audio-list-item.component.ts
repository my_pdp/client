import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';

import { AudioFIle } from 'src/app/shared/models/audio-file';

@Component({
  selector: 'app-audio-list-item',
  templateUrl: './audio-list-item.component.html',
  styleUrls: ['./audio-list-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AudioListItemComponent implements OnInit {

  @Input()
  audio: AudioFIle;

  @Input()
  playingAudioId$: number | null;

  @Output()
  toPlay = new EventEmitter<AudioFIle>();

  @Output()
  toPause = new EventEmitter<AudioFIle>();

  get isPlaying() {
    return this.audio ? this.playingAudioId$ === this.audio.id : false;
  }

  constructor(
  ) { }

  ngOnInit() {
  }

  /**
   * Change play state
   */
  changePlayState() {
    if (this.isPlaying) {
      this.pause();
    } else {
      this.play();
    }
  }

  /**
   * Set play state
   */
  play() {
    this.toPlay.emit(this.audio);
  }

  /**
   * Set pause state
   */
  pause() {
    this.toPause.emit(this.audio);
  }
}
