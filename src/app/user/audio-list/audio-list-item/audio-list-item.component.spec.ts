import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AudioListItemComponent } from './audio-list-item.component';
import { AudioFIle } from 'src/app/shared/models/audio-file';

describe('AudioListItemComponent', () => {
  let component: AudioListItemComponent;
  let fixture: ComponentFixture<AudioListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AudioListItemComponent
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AudioListItemComponent);
    component = fixture.componentInstance;

    component.audio = new AudioFIle();

    fixture.detectChanges();

    spyOn(component, 'play');
    spyOn(component, 'pause');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // changePlayState
  describe('changePlayState()', () => {

    it('should call pause() if audio is playing', () => {
      spyOnProperty(component, 'isPlaying').and.returnValue(true);

      component.changePlayState();

      expect(component.pause).toHaveBeenCalled();
    });

    it('should call play() if audio is not playing', () => {
      spyOnProperty(component, 'isPlaying').and.returnValue(false);

      component.changePlayState();

      expect(component.play).toHaveBeenCalled();
    });
  });
});
